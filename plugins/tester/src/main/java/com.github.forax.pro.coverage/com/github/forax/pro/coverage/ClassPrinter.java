package com.github.forax.pro.coverage;


import java.lang.module.ModuleReference;
import java.lang.module.ModuleReader;
import java.util.stream.Collectors;
import java.util.Objects;
import java.io.IOException;
import org.objectweb.asm.*;

public class ClassPrinter extends ClassVisitor{
	
	public ClassPrinter() {
		super(Opcodes.ASM7);
	}
	
	public void visit(int version, int access, String name,String signature, String superName, String[] interfaces) {
		System.out.println(name + " extends " + superName + " {");
	}
	
	public void visitSource(String source, String debug) {
		System.out.println("Source:  " + debug);
	}
	
	public void visitOuterClass(String owner, String name, String desc) {
		System.out.println("visitOuterClass:  " + name);
	}
	
	public AnnotationVisitor visitAnnotation(String desc,boolean visible) {
		return null;
	}
	
	public void visitAttribute(Attribute attr) {
		
	}
	
	
	public void visitInnerClass(String name, String outerName, String innerName, int access) {
		
	}
	
	public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
		System.out.println("    " + desc + " " + name);
		return null;
	}
	
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		System.out.println("    " + name + desc);
		return null;
	}

	public void visitEnd() {
		System.out.println("}");
	}
	
	public static void coverage(ModuleReference moduleRef) {
		Objects.requireNonNull(moduleRef);
		
		var cp = new ClassPrinter();
		var dbg = "";
		try{
			var m = moduleRef.open();
		
			var tab = m.list().filter(e -> e.toString().endsWith(".class")).map(e -> e.toString().split(".class")[0].replace('/', '.')).collect(Collectors.toList());
			for(var el: tab){
				dbg = el.toString();
				ClassReader cr = new ClassReader(el.toString());
				cr.accept(cp, 0);
			}
		}catch(IOException e){
			System.out.println(" IOException in ClassPrinter whith the class " + dbg + " in the module " + moduleRef.descriptor().name());
		}
	}

}
