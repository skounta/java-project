package com.github.forax.pro.coverage;


import java.io.IOException;

import org.objectweb.asm.*;

public class Main {
  public static void main(String[] args) throws IOException {
		var cp = new ClassPrinter();
		ClassReader cr = new ClassReader("java.lang.Runnable");
		cr.accept(cp, 0);
	}
}
